---
# File common.yml
# This file contains common stuff

# Include basic files
include:
  - /.gitlab/base_templates/helpers.yml
  - /.gitlab/base_templates/stages.yml
  - /.gitlab/base_templates/variables.yml
  - /.gitlab/base_templates/workflow.yml

variables:
  KQE_MARDOWNLINT_CONTAINER_IMAGE: $CKI_TOOLS_CONTAINER_IMAGE
  KQE_MARDOWNLINT_CONTAINER_VERSION: $CKI_TOOLS_CONTAINER_VERSION
  KQE_SETUP_CONTAINER_IMAGE: $KQE_TOOLS_CONTAINER_IMAGE
  KQE_SETUP_CONTAINER_VERSION: $KQE_TOOLS_CONTAINER_VERSION
  KQE_YAMLLINT_CONTAINER_IMAGE: $CKI_TOOLS_CONTAINER_IMAGE
  KQE_YAMLLINT_CONTAINER_VERSION: $CKI_TOOLS_CONTAINER_VERSION
  KQE_INTERNAL_HOSTS_CONTAINER_IMAGE: $CKI_TOOLS_CONTAINER_IMAGE
  KQE_INTERNAL_HOSTS_CONTAINER_VERSION: $CKI_TOOLS_CONTAINER_VERSION
  ALLOWED_HOSTS_TXT: "https://gitlab.com/redhat/centos-stream/src/kernel/documentation/-/raw/main/info/allowed-hosts.txt"

.kqe_markdownlint:
  image: $KQE_MARDOWNLINT_CONTAINER_IMAGE:$KQE_MARDOWNLINT_CONTAINER_VERSION
  stage: lint
  before_script:
    - !reference [.set_check_only_mr_changes, before_script]
    - jq --version
    - markdownlint --version
  script:
    - |
      if [ "${CHECK_ONLY_MR_CHANGES}" == "true" ]; then
        echo "${MSG_CHECKING_MR_CHANGES}"
        readarray -t files < <(jq -r '.data.markdown_files[]' "${CI_JSON_FILE}")
      else
        echo "${MSG_CHECKING_ALL_REPOSITORY}"
        readarray -t files < <(find . -type f -iname '*.md')
      fi

      error=0

      for file in "${files[@]}"; do
          echo "Checking markdown file ${file}"
          if ! markdownlint "${file}"; then
            error=1
          fi
      done

      if [ "${error}" -ne "0" ]; then
        echo "Please, fix the syntax problems as separate commit if they were not caused by your changes."
        exit 1
      fi

.kqe_setup:
  image: $KQE_SETUP_CONTAINER_IMAGE:$KQE_SETUP_CONTAINER_VERSION
  stage: setup
  before_script:
    - python3 --version
  script: |
      mkdir -p "${ARTIFACTS_DIR}"
      get_repository_report --working-dir ./ \
        --first-commit "${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:-${CI_COMMIT_SHA}}" \
        --last-commit "${CI_MERGE_REQUEST_DIFF_BASE_SHA}" \
        --output-file "${CI_JSON_FILE}"
  artifacts:
    paths:
      - $ARTIFACTS_DIR
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

.kqe_yamllint:
  image: $KQE_YAMLLINT_CONTAINER_IMAGE:$KQE_YAMLLINT_CONTAINER_VERSION
  stage: lint
  before_script:
    - !reference [.set_check_only_mr_changes, before_script]
    - jq --version
    - yamllint --version
  script:
    - |
      if [ "${CHECK_ONLY_MR_CHANGES}" == "true" ]; then
        echo "${MSG_CHECKING_MR_CHANGES}"
        readarray -t files < <(jq -r '.data.yaml_files[]' "${CI_JSON_FILE}")
      else
        echo "${MSG_CHECKING_ALL_REPOSITORY}"
        readarray -t files < <(find . -type f -iname '*.yaml' -o -iname '*.yml')
      fi

      error=0

      for file in "${files[@]}"; do
          echo "Checking yaml file ${file}"
          if ! yamllint "${file}"; then
            error=1
          fi
      done

      if [ "${error}" -ne "0" ]; then
        echo "Please, fix the syntax problems as separate commit if they were not caused by your changes."
        exit 1
      fi

.kqe_internal_hostname_check:
  image: $KQE_INTERNAL_HOSTS_CONTAINER_IMAGE:$KQE_INTERNAL_HOSTS_CONTAINER_VERSION
  stage: test
  script:
    - |
      curl --config "${CKI_CURL_CONFIG_FILE}" -o .allowed-hosts "${ALLOWED_HOSTS_TXT}"
      if [[ ! -f .allowed-hosts ]]; then
          echo "Error: failed to fetch allowed-hosts from ${ALLOWED_HOSTS_TXT}"
          exit 1
      fi
      readarray -t allowed_hosts < <(sed -e '/^#/d' -e 's/\s*#.*//' .allowed-hosts)
      readarray -t internal_hosts < <(grep -hroE --exclude=.allowed-hosts '([a-zA-Z0-9\\.\\-]+\.redhat.com)' * | sort -u)
      rm -f .allowed-hosts
      fail=0
      for host in "${internal_hosts[@]}"; do
        allowed=0
        for allowed_host in "${allowed_hosts[@]}"; do
          if grep -E -w -q "${allowed_host}" <<< "${host}"; then
              allowed=1
              break
          fi
        done
        if [[ "${allowed}" -eq 0 ]]; then
            echo "FAIL: hostname ${host} is not allowed.  For details, refer to: ${ALLOWED_HOSTS_TXT}"
            fail=1
        fi
      done
      exit "${fail}"
